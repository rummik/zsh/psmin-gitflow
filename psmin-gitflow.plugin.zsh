#!/bin/zsh

function @psmin-gitflow() {
	setopt localoptions extendedglob
	local line
	read line
	print ${line/(#b)([a-z])[a-z]#\/([A-Z]#-[0-9]#|[0-9]#)[\/-][a-z0-9-]#/${(j:/:)match}}
}

() {
	local gpi_orig="_gpi_"$$"_git_prompt_info"

	if ! functions $gpi_orig >/dev/null; then
		eval "function _gpi_"$$"_$(functions git_prompt_info)"
	fi

	function git_prompt_info {
		local gpi_orig="_gpi_"$$"_git_prompt_info"
		if [[ -v _PS1$$ ]]; then
			$gpi_orig | @psmin-gitflow
		else
			$gpi_orig
		fi
	}
}
